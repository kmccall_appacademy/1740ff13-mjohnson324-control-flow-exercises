# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char { |ch| str.delete!(ch) if ch == ch.downcase }
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid_point = str.length / 2
  return str[mid_point] if str.length.odd?
  str[mid_point - 1..mid_point]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowel_count = 0
  str.each_char do |ch|
    vowel_count += 1 if VOWELS.include?(ch.downcase)
  end

  vowel_count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  result = 1
  (1..num).each { |number| result *= number }
  result
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined = ""
  arr.each_index do |index|
    joined += arr[index]
    joined += separator unless index == arr.length - 1
  end

  joined
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird_word = str.downcase.chars
  weird_word.each_index do |i|
    weird_word[i] = weird_word[i].upcase if i.odd?
  end

  weird_word.join
end

# Reverse all words of five or more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  long_words_reversed = str.split
  long_words_reversed.each_index do |idx|
    if long_words_reversed[idx].length > 4
      long_words_reversed[idx] = long_words_reversed[idx].reverse
    end
  end

  long_words_reversed.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fizzbuzzed = []
  (1..n).each do |number|
    if number % 15 == 0
      fizzbuzzed << "fizzbuzz"
    elsif number % 5 == 0
      fizzbuzzed << "buzz"
    elsif number % 3 == 0
      fizzbuzzed << "fizz"
    else
      fizzbuzzed << number
    end
  end
  fizzbuzzed
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed = []
  arr.each { |element| reversed.unshift(element) }
  reversed
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2..num / 2).each { |number| return false if num % number == 0 }
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factor_list = []
  (1..num).each { |number| factor_list << number if num % number == 0 }
  factor_list
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  primes = []
  factors(num).each { |factor| primes << factor if prime?(factor) }
  primes
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odds = []
  evens = []

  arr.each do |number|
    if number.even?
      evens << number
    else
      odds << number
    end
  end

  select_oddball(odds, evens)
end

def select_oddball(odds, evens)
  if evens.length > odds.length
    odds[0]
  else
    evens[0]
  end
end
